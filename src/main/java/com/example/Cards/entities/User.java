package com.example.Cards.entities;

import com.example.Cards.annotations.PhoneNumber;
import com.example.Cards.utils.UserRole;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "\"User\"")
public class User implements UserDetails {

    public static final String REGEXP_FOR_EMAIL = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    public static final String EMAIL_IS_NOT_VALID_MESSAGE = "Email is not valid";
    public static final String PHONE_NUMBER_IS_NOT_VALID = "PhoneNumber is not valid";
    public static final String REGEXP_FOR_PHONE_NUMBER = "^(\\+\\d{1,3}[- ]?)?\\d{10}$";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotBlank(message = "Name must not be blank")
    private String name;
    @NotBlank(message = "Surname must not be blank")
    private String surname;
    @NotBlank(message = "Email must not be blank")
    @Email(message = EMAIL_IS_NOT_VALID_MESSAGE, regexp = REGEXP_FOR_EMAIL)
    @Column(unique = true)
    private String email;
    @NotBlank(message = "PhoneNumber must not be blank")
    @PhoneNumber(message = PHONE_NUMBER_IS_NOT_VALID, regexp = REGEXP_FOR_PHONE_NUMBER)
    private String phoneNumber;
    @NotBlank(message = "Password must not be blank")
    private String password;
    private UserRole role;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}