package com.example.Cards.entities;

import com.example.Cards.annotations.CardColor;
import com.example.Cards.utils.CardStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Card {

    public static final String COLOR_FORMAT_MESSAGE = "Color must be or empty or in format of 6 alphanumerical characters prefixed with #";
    public static final String REGEXP_FOR_COLOR = "^#([A-Fa-f0-9]{6})$";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotBlank(message = "Name must not be blank")
    private String name;
    private String description;
    @CardColor(message = COLOR_FORMAT_MESSAGE, regexp = REGEXP_FOR_COLOR)
    private String color;
    private CardStatus status;
    @Column(updatable = false)
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = User.class)
    private User user;
}