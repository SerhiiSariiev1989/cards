package com.example.Cards.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@ResponseStatus
public class ExceptionHandler extends ResponseEntityExceptionHandler {
    @org.springframework.web.bind.annotation.ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorMessage> userNotFoundException(UserNotFoundException exception, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND, exception.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(errorMessage);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(CardNotFoundException.class)
    public ResponseEntity<ErrorMessage> cardNotFoundException(CardNotFoundException exception, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND, exception.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(errorMessage);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(CreationDateChangeException.class)
    public ResponseEntity<ErrorMessage> creationDateChangeException(CreationDateChangeException exception, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.FORBIDDEN, exception.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(errorMessage);
    }
}
