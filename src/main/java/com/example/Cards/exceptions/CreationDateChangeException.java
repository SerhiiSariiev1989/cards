package com.example.Cards.exceptions;

public class CreationDateChangeException extends RuntimeException {

    public CreationDateChangeException() {
        super();
    }

    public CreationDateChangeException(String message) {
        super(message);
    }

    public CreationDateChangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreationDateChangeException(Throwable cause) {
        super(cause);
    }
}
