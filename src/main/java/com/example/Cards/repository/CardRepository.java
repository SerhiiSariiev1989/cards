package com.example.Cards.repository;

import com.example.Cards.entities.Card;
import com.example.Cards.utils.CardStatus;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface CardRepository extends JpaRepository<Card, Integer> {
    List<Card> findByUserEmailOrderByCreationDateDesc(String email);

    @Query("select c from Card c where c.user.email = :userEmail" +
            " AND(:name is null or c.name = :name)" +
            " AND(:color is null or c.color = :color)" +
            " AND(:status is null or c.status = :status)" +
            " AND(:creationDate is null or c.creationDate = :creationDate)")
    List<Card> findBySearchCriteria(@Param("userEmail") String userEmail,
                                    @Param("name") String name,
                                    @Param("color") String color,
                                    @Param("status") CardStatus status,
                                    @Param("creationDate") Date creationDate,
                                    Sort orderBy);
}
