package com.example.Cards.service;

import com.example.Cards.entities.Card;

import java.util.List;
import java.util.Map;

public interface CardService {
    Card saveCard(Card card);

    Card getCardById(Integer id);

    List<Card> getAllCards();

    Card updateCard(Card card);

    void deleteCardById(Integer id);

    List<Card> getCardBySearchCriteria(Map<String, Object> searchCriteria);
}
