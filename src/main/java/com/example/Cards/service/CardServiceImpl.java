package com.example.Cards.service;

import com.example.Cards.entities.Card;
import com.example.Cards.exceptions.CardNotFoundException;
import com.example.Cards.repository.CardRepository;
import com.example.Cards.utils.CardStatus;
import com.example.Cards.utils.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private UserService userService;

    @Override
    public Card saveCard(Card card) {
        String authorizedUserEmail = getAuthorizedUserEmail();
        com.example.Cards.entities.User user = userService.getUserByEmail(authorizedUserEmail);
        card.setUser(user);
        card.setStatus(CardStatus.TO_DO);
        card.setCreationDate(new Date(System.currentTimeMillis()));
        return cardRepository.save(card);
    }

    @Override
    public Card getCardById(Integer id) {
        return cardRepository.findById(id).orElseThrow(() -> new CardNotFoundException("Card with id = " + id + " is not found"));
    }

    @Override
    public List<Card> getAllCards() {
        return cardRepository.findAll();
    }

    @Override
    public Card updateCard(Card card) {
        Card cardFromDb = getCardById(card.getId());
        checkIfAuthorized(cardFromDb);
        validateRequestParameters(card, cardFromDb);
        Card updatedCard = Card.builder()
                .id(cardFromDb.getId())
                .name(card.getName())
                .description(card.getDescription())
                .color(card.getColor())
                .status(card.getStatus())
                .creationDate(cardFromDb.getCreationDate())
                .user(cardFromDb.getUser())
                .build();
        return cardRepository.save(updatedCard);
    }

    private static void validateRequestParameters(Card card, Card cardFromDb) {
        if (card.getName() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Name must be not empty");
        }
        if (card.getStatus() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Status must be not empty");
        }
    }

    @Override
    public void deleteCardById(Integer id) {
        checkIfAuthorized(getCardById(id));
        cardRepository.deleteById(id);
    }

    @Override
    public List<Card> getCardBySearchCriteria(Map<String, Object> searchCriteria) {
        String authorizedUserEmail = getAuthorizedUserEmail();
        String status = (String) searchCriteria.get("status");
        return cardRepository.findBySearchCriteria(authorizedUserEmail,
                (String) searchCriteria.get("name"),
                (String) searchCriteria.get("color"),
                status != null ? CardStatus.valueOf(status) : null,
                getCreationDate((String) searchCriteria.get("creationDate")),
                Sort.by(((String) searchCriteria.get("orderBy")).split(", ")));
    }

    private static java.util.Date getCreationDate(String creationDate) {
        if (creationDate == null) {
            return null;
        }
        try {
            return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(creationDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getAuthorizedUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null ? ((User) authentication.getPrincipal()).getUsername() : null;
    }

    private static String getAuthorizedUserAuthorities() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return String.valueOf(((User) authentication.getPrincipal()).getAuthorities());
    }

    private static void checkIfAuthorized(Card card) {
        String email = card.getUser().getEmail();
        String authorizedUserEmail = getAuthorizedUserEmail();
        if ((authorizedUserEmail == null || !authorizedUserEmail.equals(email)) && !getAuthorizedUserAuthorities().contains(UserRole.ADMIN.toString())) {
            throw new AccessDeniedException("403 returned");
        }
    }
}