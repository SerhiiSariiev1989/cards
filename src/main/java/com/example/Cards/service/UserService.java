package com.example.Cards.service;

import com.example.Cards.exceptions.UserNotFoundException;
import com.example.Cards.entities.User;

import java.util.List;

public interface UserService {

    User saveUser(User user);

    User getUserById(Integer id) throws UserNotFoundException;

    User getUserByEmail(String email) throws UserNotFoundException;

    User updateUser(User user);

    void deleteUserById(Integer id);

    List<User> getAllUsers();
}