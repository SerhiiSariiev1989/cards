package com.example.Cards.controller;

import com.example.Cards.entities.User;
import com.example.Cards.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/saveUser")
    public User saveUser(@RequestBody User user) {
        log.info("Save user");
        return userService.saveUser(user);
    }

//    @GetMapping("/user/{id}")
//    public User getUserById(@PathVariable Integer id) {
//        log.info("Get user by id");
//        return userService.getUserById(id);
//    }

    @GetMapping("/user/{email}")
    public User getUserByEmail(@PathVariable String email) {
        log.info("Get user by email");
        return userService.getUserByEmail(email);
    }

    @GetMapping("/getAllUsers")
    public List<User> getAllUsers() {
        log.info("Get all users");
        return userService.getAllUsers();
    }

    @PutMapping("/updateUser")
    public User updateUser(@RequestBody User user) {
        log.info("Update user");
        return userService.updateUser(user);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUserById(@PathVariable Integer id) {
        log.info("Delete user");
        userService.deleteUserById(id);
    }
}