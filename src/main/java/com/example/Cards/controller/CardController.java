package com.example.Cards.controller;

import com.example.Cards.entities.Card;
import com.example.Cards.service.CardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class CardController {

    @Autowired
    private CardService cardService;

    @PostMapping("/saveCard")
    public Card saveCard(@RequestBody Card card) {
        log.info("Save card");
        return cardService.saveCard(card);
    }

    @GetMapping("/getCardsBySearchCriteria")
    public List<Card> getCardsBySearchCriteria(@RequestBody Map<String, Object> searchCriteria) {
        log.info("Get card by search criteria");
        return cardService.getCardBySearchCriteria(searchCriteria);
    }

    @GetMapping("/getAllCards")
    public List<Card> getAllCards() {
        log.info("Get all cards");
        return cardService.getAllCards();
    }

    @PutMapping("/updateCard")
    public Card updateCard(@RequestBody Card card) {
        log.info("Update card");
        return cardService.updateCard(card);
    }

    @DeleteMapping("/{id}")
    public void deleteCardById(@PathVariable Integer id) {
        log.info("Delete card");
        cardService.deleteCardById(id);
    }
}
