package com.example.Cards.utils;

public enum UserRole {
    MEMBER, ADMIN
}
