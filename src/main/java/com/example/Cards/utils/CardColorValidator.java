package com.example.Cards.utils;

import com.example.Cards.annotations.CardColor;
import io.micrometer.common.util.StringUtils;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.regex.PatternSyntaxException;

public class CardColorValidator implements ConstraintValidator<CardColor, String> {

    private static final Log LOG = LoggerFactory.make(MethodHandles.lookup());
    private java.util.regex.Pattern pattern;


    @Override
    public void initialize(CardColor constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        if (!".*".equals(constraintAnnotation.regexp())) {
            try {
                pattern = java.util.regex.Pattern.compile(constraintAnnotation.regexp());
            }
            catch (PatternSyntaxException e) {
                throw LOG.getInvalidRegularExpressionException(e);
            }
        }
    }

    @Override
    public boolean isValid(String cardColor, ConstraintValidatorContext context) {
        return StringUtils.isBlank(cardColor) || pattern.matcher(cardColor).matches();
    }
}
