package com.example.Cards.utils;

import com.example.Cards.annotations.PhoneNumber;
import io.micrometer.common.util.StringUtils;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.regex.PatternSyntaxException;

public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {

    private static final Log LOG = LoggerFactory.make(MethodHandles.lookup());
    private java.util.regex.Pattern pattern;

    @Override
    public void initialize(PhoneNumber phoneNumberAnnotation) {
        ConstraintValidator.super.initialize(phoneNumberAnnotation);
        if (!".*".equals(phoneNumberAnnotation.regexp())) {
            try {
                pattern = java.util.regex.Pattern.compile(phoneNumberAnnotation.regexp());
            }
            catch (PatternSyntaxException e) {
                throw LOG.getInvalidRegularExpressionException(e);
            }
        }
    }

    @Override
    public boolean isValid(String phoneNumber, ConstraintValidatorContext context) {
        return StringUtils.isNotBlank(phoneNumber) && pattern.matcher(phoneNumber).matches();
    }
}
