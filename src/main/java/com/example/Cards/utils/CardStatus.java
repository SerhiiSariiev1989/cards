package com.example.Cards.utils;

public enum CardStatus {
    DONE, IN_PROGRESS, TO_DO
}
